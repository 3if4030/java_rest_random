package if4030.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class RandomApplication {
	
	private int min = 1;
	private int max = 100;
	
	@GetMapping("/nextRandom")
	public String nextRandom() {
	    System.out.println("nextRandom called");
        double alea = Math.random();
        return "" + (min + ( int ) (( max - min + 1 ) * alea ));
	}


	@GetMapping("/setBounds")
	public void setBounds(
			@RequestParam(value = "min", defaultValue = "1") String min,
			@RequestParam(value = "max", defaultValue = "100") String max) {
	    System.out.println("setBounds called");
		try {
			int newMin = Integer.valueOf(min);
			int newMax = Integer.valueOf(max);
			if (newMin <= newMax) {
				this.min = newMin;
				this.max = newMax;
			}
		}
		catch(NumberFormatException e) {}
	}


	public static void main(String[] args) {
		SpringApplication.run(RandomApplication.class, args);
	}

}
